package com.teste;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import com.google.gson.Gson;
import com.teste.controller.TotalController;
import com.teste.models.Total;

@SpringBootApplication
public class FelipeApplication {

	public static void main(String[] args) throws IOException, ParseException {

		SpringApplication.run(FelipeApplication.class, args);

	}

	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}

}
