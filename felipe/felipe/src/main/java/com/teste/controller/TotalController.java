package com.teste.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.teste.models.Total;

@Controller
public class TotalController {
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView listaTotal(@RequestParam String language) throws URISyntaxException, IOException {
		
		URL api = new URL("https://api.github.com/search/repositories?"
				  +"&api_key=241a0cffbc9dda0368afdeb9efa04fdcc9ce4570"
				  +"&q=language:"+language 
				  +"&sort=stars"
				  //+"&page="+page
				  +"&per_page=40");
		
		Gson gson = new Gson();
		Total tc = new Total();
		ModelAndView mv = new ModelAndView("index");
		
		try {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(api.openStream()));
		String urlString = in.readLine();
		
		tc = gson.fromJson(urlString, Total.class);

		mv.addObject("lista", tc.getItems());
		
		} catch (IOException e) {
			
		}
		
		return mv;
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView listaTotal() throws URISyntaxException {

		ModelAndView mv = new ModelAndView("welcome");
	
		return mv;

	}
}
