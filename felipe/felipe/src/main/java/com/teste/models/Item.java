package com.teste.models;

public class Item {

	private String full_name;
	private Owner owner;
	private int stargazers_count;
	private int forks;

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public int getStargazers_count() {
		return stargazers_count;
	}

	public void setStargazers_count(int stargazers_count) {
		this.stargazers_count = stargazers_count;
	}
	
	public int getForks() {
		return forks;
	}

	public void setForks(int forks) {
		this.forks = forks;
	}

}
