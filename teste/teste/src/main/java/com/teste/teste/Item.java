package com.teste.teste;

import java.util.List;

public class Item {

	private String name;
	
	private List<Owner> login;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Owner> getLogin() {
		return login;
	}
	public void setLogin(List<Owner> login) {
		this.login = login;
	}

}
